import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Layouts 1.12
import QtQuick.Controls 2.12
import QtQuick.Dialogs 1.2
import QtQuick.Controls.Material 2.12

ApplicationWindow {
    id: applicationWindow
    visible: true
    width: 640
    height: 480
    onClosing: { close.accepted = false; }
    Keys.onReleased: {
        if (event.key === Qt.Key_Back) {
            mainStackLayout.backHandler()
        }
    }

    FontLoader {
        id: materialIconFont
        source: "qrc:/MaterialIcons-Regular.ttf"
    }
    MessageDialog {
        id: errorDialog
    }
    StackLayout {
        id: mainStackLayout
        anchors.top: tabBar.bottom
        anchors.bottom: parent.bottom
        anchors.right: tabBar.right
        x: 0
        width: parent.width
        anchors.topMargin: 0
        currentIndex: tabBar.currentIndex
        function refreshHandler() {
            switch (currentIndex) {
            case 0:
                FSRootBackingModel.refresh();
                break;
            case 1:
                FSInternalBackingModel.refresh();
                break;
            case 2:
                FSExternalBackingModel.refresh();
                break;
            }
        }
        function backHandler() {
            switch (currentIndex) {
            case 0:
                FSRootBackingModel.openDirectory("..", true);
                textLocation.text = FSRootBackingModel.currentLocation();
                break;
            case 1:
                FSInternalBackingModel.openDirectory("..", true);
                textLocation.text = FSInternalBackingModel.currentLocation();
                break;
            case 2:
                FSExternalBackingModel.openDirectory("..", true);
                textLocation.text = FSExternalBackingModel.currentLocation();
                break;
            }
        }
        function indexChangedHandler() {
            switch (currentIndex) {
            case 0:
                textLocation.text = FSRootBackingModel.currentLocation();
                break;
            case 1:
                textLocation.text = FSInternalBackingModel.currentLocation();
                break;
            case 2:
                textLocation.text = FSExternalBackingModel.currentLocation();
                break;
            }
        }
        function goHandler() {
            switch (currentIndex) {
            case 0:
                FSRootBackingModel.openDirectory(textLocation.text)
                textLocation.text = FSRootBackingModel.currentLocation();
                break;
            case 1:
                FSInternalBackingModel.openDirectory(textLocation.text)
                textLocation.text = FSInternalBackingModel.currentLocation();
                break;
            case 2:
                FSExternalBackingModel.openDirectory(textLocation.text)
                textLocation.text = FSExternalBackingModel.currentLocation();
                break;
            }
        }

        Component.onCompleted: {
            indexChangedHandler();
        }
        onCurrentIndexChanged: {
            indexChangedHandler();
        }
        function itemPressedHandler(model,path,isDirectory) {
            if (isDirectory) {
                model.openDirectory(path, true);
                textLocation.text = model.currentLocation();
            } else {
                model.openFile(path);
            }
        }

        ScrollView {
            clip: true
            ListView {
                width: parent.width
                model: FSRootBackingModel
                    FileEntryDelegate {
                    fileEntryName: model.fileName
                    fileSize: model.fileSize
                    isDirectory: model.isDirectory
                    width: parent.width
                    onClicked: {
                        mainStackLayout.itemPressedHandler(FSRootBackingModel, fileEntryName, isDirectory);
                    }
                    onIsCheckedChanged: {
                        model.isChecked = true;
                    }
                }
            }
        }
        ScrollView {
            clip: true
            ListView {
                width: parent.width
                model: FSInternalBackingModel
                delegate: FileEntryDelegate {
                    fileEntryName: model.fileName
                    fileSize: model.fileSize
                    isDirectory: model.isDirectory
                    width: parent.width
                    onClicked: {
                        mainStackLayout.itemPressedHandler(FSInternalBackingModel, fileEntryName, isDirectory);
                    }
                }
            }
        }
        ScrollView {
            clip: true
            ListView {
                width: parent.width
                model: FSExternalBackingModel
                delegate: FileEntryDelegate {
                    fileEntryName: model.fileName
                    fileSize: model.fileSize
                    isDirectory: model.isDirectory
                    isChecked: model.isChecked
                    width: parent.width
                    onClicked: {
                        mainStackLayout.itemPressedHandler(FSExternalBackingModel, FSExternalBackingModel.filenameAt(model.index), isDirectory);
                    }
                }
            }
        }
    }


    TabBar {
        id: tabBar
        x: 0
        width: parent.width
        height: 48
        position: TabBar.Header
        anchors.top: actionBar.bottom
        anchors.topMargin: 0
        currentIndex: 2
        TabButton {
            text: qsTr("Root")
        }
        TabButton {
            text: qsTr("Internal Storage")
        }
        TabButton {
            text: qsTr("External Storage")
        }
    }

    ToolBar {
        id: actionBar
        height: 0
        visible: false
        anchors.top: navigationBar.bottom
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        Material.background: "#cccccc"

        Button {
            id: buttonRename
            y: 6
            width: 61
            height: 60
            text: qsTr("\ue3c9")
            anchors.left: buttonInfo.right
            anchors.leftMargin: 0
            anchors.verticalCenterOffset: 0
            anchors.verticalCenter: parent.verticalCenter
            font.family: materialIconFont.name
            font.pointSize: 29
            Material.foreground: "black"
            flat: true
        }

        Button {
            id: buttonDelete
            y: 3
            width: 63
            height: 61
            text: qsTr("\ue92b")
            anchors.left: buttonRename.right
            anchors.leftMargin: 0
            anchors.verticalCenter: parent.verticalCenter
            font.pointSize: 29
            Material.foreground: "black"
            font.family: materialIconFont.name
            flat: true
        }

        Button {
            id: buttonCopy
            y: 3
            width: 61
            height: 61
            text: qsTr("\ue14d")
            anchors.left: buttonDelete.right
            anchors.leftMargin: 0
            font.pointSize: 29
            Material.foreground: "black"
            font.family: materialIconFont.name
            flat: true
        }

        Button {
            id: buttonInfo
            x: 0
            y: 2
            width: 61
            height: 61
            text: qsTr("\ue88e")
            font.pointSize: 29
            Material.foreground: "black"
            font.family: materialIconFont.name
            flat: true
        }

        Label {
            id: label
            x: 337
            y: 10
            color: "#000000"
            text: qsTr("0 Items Selected")
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: 13
            font.pointSize: 23
        }

        Button {
            id: buttonCut
            y: 0
            width: 63
            height: 61
            text: qsTr("\ue14e")
            anchors.left: buttonCopy.right
            anchors.leftMargin: 0
            anchors.verticalCenterOffset: 1
            font.pointSize: 29
            Material.foreground: "black"
            font.family: materialIconFont.name
            flat: true
            anchors.verticalCenter: parent.verticalCenter
        }
    }

    ToolBar {
        id: navigationBar
        height: 66
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        Button {
            id: btnBack
            y: 0
            width: 50
            height: 66
            text: qsTr("\ue5c4")
            anchors.left: parent.left
            anchors.leftMargin: 7
            display: AbstractButton.TextOnly
            font.family: materialIconFont.name
            font.pointSize: 29
            highlighted: false
            flat: true
            onClicked: mainStackLayout.backHandler();
        }

        Button {
            id: btnRefresh
            y: 0
            width: 50
            height: 66
            text: qsTr("\ue5d5")
            anchors.left: btnBack.right
            anchors.leftMargin: 6
            display: AbstractButton.TextOnly
            font.family: materialIconFont.name
            highlighted: false
            flat: true
            font.pointSize: 29
            onClicked: mainStackLayout.refreshHandler();
        }

        TextField {
            id: textLocation
            y: 0
            height: 66
            text: qsTr("")
            font.pointSize: 29
            verticalAlignment: Text.AlignVCenter
            anchors.right: btnGo.left
            anchors.rightMargin: 11
            anchors.left: btnRefresh.right
            anchors.leftMargin: 6
        }

        Button {
            id: btnGo
            x: 580
            y: 0
            width: 50
            height: 66
            text: qsTr("\ue154")
            anchors.right: parent.right
            anchors.rightMargin: 10
            display: AbstractButton.TextOnly
            font.family: materialIconFont.name
            highlighted: false
            flat: true
            font.pointSize: 29
            onClicked: mainStackLayout.goHandler();
        }
    }
}

/*##^##
Designer {
    D{i:13;anchors_y:53}D{i:18;anchors_x:71}D{i:19;anchors_x:67}D{i:20;anchors_width:450;anchors_x:207}
D{i:21;anchors_width:50;anchors_x:580}D{i:23;anchors_width:50;anchors_x:274}D{i:17;anchors_width:640;anchors_x:201;anchors_y:0}
D{i:25;anchors_width:50;anchors_x:580}D{i:26;anchors_width:50;anchors_x:580}D{i:27;anchors_width:50;anchors_x:580}
D{i:24;anchors_width:50;anchors_x:580}
}
##^##*/
