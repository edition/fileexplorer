#include <QFile>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QDir>
#ifdef Q_OS_ANDROID
#include <QtAndroidExtras>
#include <QAndroidJniObject>
#endif
#include "abstract_file_entry_model.hxx"

using BackingModelType = AbstractFileEntryModel;

#define STATIC_WRAPPER_AUTO_INITIALIZE(variable_name)     \
  BackingModelType* variable_name() {                     \
    static BackingModelType* g_##variable_name;            \
    if (g_##variable_name) {                              \
      return g_##variable_name;                           \
    } else {                                              \
      g_##variable_name = new BackingModelType;           \
      return g_##variable_name;                           \
    }                                                     \
  }

STATIC_WRAPPER_AUTO_INITIALIZE(root_backing_model)
STATIC_WRAPPER_AUTO_INITIALIZE(external_backing_model)
STATIC_WRAPPER_AUTO_INITIALIZE(internal_backing_model)

void populate_backing_model_with_path(const QString& modelName) {
  using JniObj = QAndroidJniObject;
  BackingModelType* targetModel = nullptr;
  QString currentPath;
  if (modelName == "root") {
    targetModel = root_backing_model();
    currentPath = "/";
  } else if (modelName == "external") {
    targetModel = external_backing_model();
    currentPath = "/sdcard";
  } else if (modelName == "internal") {
    targetModel = internal_backing_model();
    JniObj file = JniObj
        ::callStaticObjectMethod("android/os/Environment",
                                 "getDataDirectory",
                                 "()Ljava/io/File;");
    currentPath = file.callObjectMethod<jstring>("getPath").toString();
  }
  targetModel->openDirectory(currentPath, false);

}
#ifdef Q_OS_ANDROID
void ask_for_android_permissions()
{
  using namespace QtAndroid;
  const QString per = "android.permission.READ_EXTERNAL_STORAGE";
  requestPermissionsSync({per});
}
#endif

int main(int argc, char* argv[]) {
//  QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
  QGuiApplication app(argc, argv);
#ifdef Q_OS_ANDROID
  ask_for_android_permissions();
  QtAndroid::hideSplashScreen(27);
#endif
  populate_backing_model_with_path("root");
  populate_backing_model_with_path("external");
  populate_backing_model_with_path("internal");
  QQmlApplicationEngine engine;
  engine.rootContext()->setContextProperty("FSRootBackingModel", root_backing_model());
  engine.rootContext()->setContextProperty("FSExternalBackingModel", external_backing_model());
  engine.rootContext()->setContextProperty("FSInternalBackingModel", internal_backing_model());
  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated, &app,
      [url](QObject* obj, const QUrl& objUrl) {
        if (!obj && url == objUrl) QCoreApplication::exit(-1);
      },
      Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
