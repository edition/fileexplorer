win32 {
    CONFIG += PLATFORM_WIN
    message(PLATFORM_WIN)
    win32-g++ {
        CONFIG += COMPILER_GCC
        message(COMPILER_GCC)
    }
    win32-msvc2017 {
        CONFIG += COMPILER_MSVC2017
        message(COMPILER_MSVC2017)
        win32-msvc2017:QMAKE_TARGET.arch = x86_64
    }
}

linux {
  message($$QT_ARCH)
  unix:android: {
    CONFIG += PLATFORM_ANDROID
    CONFIG += COMPILER_CLANG
    message(COMPILER_CLANG)  
  } else {
    CONFIG += PLATFORM_LINUX
    CONFIG += COMPILER_GCC
  }
  QMAKE_TARGET.arch = $$QT_ARCH
}

macx {
    CONFIG += PLATFORM_OSX
    message(PLATFORM_OSX)
    macx-clang {
        CONFIG += COMPILER_CLANG
        message(COMPILER_CLANG)
        QMAKE_TARGET.arch = x86_64
    }
    macx-clang-32{
        CONFIG += COMPILER_CLANG
        message(COMPILER_CLANG)
        QMAKE_TARGET.arch = x86
    }
}

message("Target arch: $$QMAKE_TARGET.arch")

CONFIG += $$join(PROCESSOR_,,,QMAKE_TARGET.arch})

#contains(QMAKE_TARGET.arch, x86_64) {
#    CONFIG += PROCESSOR_x64
#    message(PROCESSOR_x64)
#} else {
#    contains(QMAKE_TARGET.arch, arm64) {
#      CONFIG += PROCESSOR_arm64
#      message(PROCESSOR_arm64)
#    } else {
#      contains(QMAKE_TARGET.arch, arm) {
#        CONFIG += PROCESSOR_arm
#      } else {
#        CONFIG += PROCESSOR_x86
#        message(PROCESSOR_x86)
#      }
#    }
#}


CONFIG(debug, release|debug) {
    CONFIG += BUILD_DEBUG
    message(BUILD_DEBUG)
} else {
    CONFIG += BUILD_RELEASE
    message(BUILD_RELEASE)
}
