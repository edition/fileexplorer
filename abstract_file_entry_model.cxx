#include "abstract_file_entry_model.hxx"
#include <QDir>
#include <QDebug>
#ifdef Q_OS_ANDROID
#include <QAndroidJniObject>
#include <QAndroidJniEnvironment>
#include <QtAndroidExtras>
#endif
#include <QMimeDatabase>

QHash<int, QByteArray> AbstractFileEntryModel::roleNames() const {
  QHash<int, QByteArray> roles;
  roles[FileName] = "fileName";
  roles[FileSize] = "fileSize";
  roles[IsDirectory] = "isDirectory";
  roles[IsChecked] = "isChecked";
  return roles;
}
//Based on code from https://stackoverflow.com/a/47854799/3924272
qint64 dirSize(QString dirPath) {
  qint64 size = 0;
  QDir dir(dirPath);
  //calculate total size of current directories' files
  QDir::Filters fileFilters = QDir::Files|QDir::System|QDir::Hidden;
  for(QString filePath : dir.entryList(fileFilters)) {
    QFileInfo fi(dir, filePath);
    size+= fi.size();
  }
  //add size of child directories recursively
  QDir::Filters dirFilters = QDir::Dirs|QDir::NoDotAndDotDot|QDir::System|QDir::Hidden;
  for(QString childDirPath : dir.entryList(dirFilters))
    size+= dirSize(dirPath + QDir::separator() + childDirPath);
  return size;
}
int AbstractFileEntryModel::rowCount(const QModelIndex& parent = QModelIndex()) const {
  return _entries.size();
}
QVariant AbstractFileEntryModel::data(const QModelIndex& index, int role = Qt::DisplayRole) const {
  FileEntry currentEntry = _entries[index.row()];
  switch (role) {
    case FileName:
      return currentEntry._name;
    case FileSize:
      return currentEntry._size;
    case IsDirectory:
      return currentEntry._isDirectory;
    case IsChecked:
      return currentEntry._isChecked;
  }
  return QVariant();
}
void AbstractFileEntryModel::append(FileEntry value) {
  _entries.append(value);
}
bool AbstractFileEntryModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  FileEntry currentEntry = _entries[index.row()];
  switch (role) {
    case FileName:
      currentEntry._name = value.toString();
    case FileSize:
      currentEntry._size = value.toReal();
    case IsDirectory:
      currentEntry._isDirectory = value.toBool();
    case IsChecked:
      currentEntry._isChecked = value.toBool();
      return true;
  }
  return false;
}
Qt::ItemFlags AbstractFileEntryModel::flags(const QModelIndex &index) const
{
  Q_UNUSED(index)
  return Qt::ItemIsEditable;
}
void AbstractFileEntryModel::openDirectory(QString path, bool isRelative) {
  if (isRelative) {
    _currentPath = QFileInfo(_currentPath + '/' + path).canonicalFilePath();
  } else {
    _currentPath = QFileInfo(path).canonicalFilePath();
  }
  beginRemoveRows(QModelIndex(), 0, _entries.size() - 1);
  _entries.clear();
  endRemoveRows();
  QDir dirEntries(_currentPath);
  //Excluding "." and ".." files
  beginInsertRows(QModelIndex(), 0, dirEntries.entryInfoList().size() - 3);
  for (auto file : dirEntries.entryInfoList()) {    
    if (file.fileName() == "." || file.fileName() == "..") continue;
    FileEntry entry;
    entry._name = file.fileName();
//    if (file.isDir()) {
//      //Get total size of the directory
//      entry._size = dirSize(file.path());
//    } else {
      entry._size = file.size();
//    }
    entry._isDirectory = file.isDir();
    entry._isChecked = false;
    qDebug() << entry._name;
    append(entry);
  }
  endInsertRows();
}
bool AbstractFileEntryModel::openFile(QString path)
{
  QString fullPath = QFileInfo(_currentPath + '/' + path).canonicalFilePath();
  using JniObj = QAndroidJniObject;
  QMimeDatabase mimeDb;
  QString mime = mimeDb.mimeTypeForFile(fullPath).name();
  bool isExternal = false;
  if (_currentPath.startsWith("/storage/emulated/")) {
    isExternal = true;
  }
  qDebug () << QString("Opening %1 (%2)").arg(fullPath).arg(mime);
  jboolean returnValue = JniObj::callStaticMethod<jboolean>("com/bgcottrell/FileExplorer/Utils",
                                 "openFile",
                                 "(Landroid/app/Activity;ZLjava/lang/String;Ljava/lang/String;)Z",
                                 QtAndroid::androidActivity().object(),
                                 isExternal,
                                 JniObj::fromString(fullPath).object(),
                                 JniObj::fromString(mime).object());
  if (returnValue == JNI_FALSE) {
    return false;
  }
  return true;
}
QString AbstractFileEntryModel::currentLocation()
{
  return _currentPath;
}
void AbstractFileEntryModel::refresh()
{
  openDirectory(_currentPath, false);
}
QString AbstractFileEntryModel::filenameAt(qint32 index)
{
  return _entries.at(index)._name;
}
QString AbstractFileEntryModel::readableFileSize(qint64 byteSize)
{
  qint32 unitSize = 0;
  QString unit = "";
  if (byteSize > 1000 && byteSize < 1000000) {

  }
  /*
   * //KiB
        var unitSize = 0;
        var unit = "";
        if (size > 1000 && size < 1000000 - 1) {
            unitSize = size / 1000;
            unit = "KiB";
            return `${size / 1000} KiB`
        } else if (size > 1000000 && size < 1000000000 - 1) {
            //MiB
            unitSize = size / 1000000;
            unit = "MiB";
        } else if (size > 1000000000 - 1) {
            //GiB
            unitSize = size / 1000000000;
            unit = "GiB";
        } else {
            unitSize = size;
            unit = "Bytes";
        }
        if (unit !== "Bytes") {
            return `${parseFloat(unitSize).toFixed(1)} ${unit}`
        } else {
            return `${unitSize} ${unit}`
        }*/
}
