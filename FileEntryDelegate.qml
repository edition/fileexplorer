import QtQuick 2.0
import QtQuick.Controls 2.5
import QtQuick.Controls.Styles 1.4

ItemDelegate {
    id: itemDelegate
    property string fileEntryName : ""
    property real fileSize : 0
    property bool isDirectory : false
    property bool isChecked : false
    width: 400
    height: 65

    Image {
        id: imageFileEntryType
        x: 8
        y: 9
        width: 48
        height: 48
        anchors.verticalCenterOffset: -2
        anchors.verticalCenter: parent.verticalCenter
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 11
        anchors.left: checkBox.right
        anchors.leftMargin: 4
        fillMode: Image.PreserveAspectFit
        source: isDirectory ? "seeker-folder.png" : "seeker-file.png"
    }

    Label {
        id: labelFileName
        x: 62
        y: 10
        height: 16
        text: fileEntryName
        elide: Text.ElideRight
        anchors.right: parent.right
        anchors.rightMargin: 5
        wrapMode: Text.WordWrap
        anchors.left: imageFileEntryType.right
        anchors.leftMargin: 1
        textFormat: Text.PlainText
        font.bold: false
    }

    Rectangle {
        id: rectangle
        y: 63
        height: 2
        color: isChecked ? "#00fc00" : "#555555"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
    }
    function getReadableFileSize(size)
    {
        //KiB
        var unitSize = 0;
        var unit = "";
        if (size > 1000 && size < 1000000 - 1) {
            unitSize = size / 1000;
            unit = "KiB";
            return `${size / 1000} KiB`
        } else if (size > 1000000 && size < 1000000000 - 1) {
            //MiB
            unitSize = size / 1000000;
            unit = "MiB";
        } else if (size > 1000000000 - 1) {
            //GiB
            unitSize = size / 1000000000;
            unit = "GiB";
        } else {
            unitSize = size;
            unit = "Bytes";
        }
        if (unit !== "Bytes") {
            return `${parseFloat(unitSize).toFixed(1)} ${unit}`
        } else {
            return `${unitSize} ${unit}`
        }
    }
    Label {
        id: labelFileSize
        x: 57
        y: 31
        height: 16
        text: !isDirectory ? getReadableFileSize(fileSize) : ""
        anchors.right: parent.right
        anchors.left: imageFileEntryType.right
        font.bold: true
        wrapMode: Text.WordWrap
        anchors.leftMargin: 1
        textFormat: Text.PlainText
        anchors.rightMargin: 5
    }

}

/*##^##
Designer {
    D{i:0;formeditorColor:"#000000"}D{i:2;anchors_width:288}D{i:3;anchors_width:400;anchors_x:0}
D{i:4;anchors_width:288}
}
##^##*/
